# frozen_string_literal: true

require_relative 'lib/trello/version'

Gem::Specification.new do |spec|
  spec.name = 'trello_client_lite'
  spec.version = Trello::VERSION
  spec.authors = ['Charlie Campbell']
  spec.email = ['charlie.campbell14@gmail.com']

  spec.summary = 'Client gem for Trello API'
  spec.description = 'Light touch client gem for the trello API, current work in ' \
                     'progress. Designed for personal use in university project.'
  spec.homepage = 'https://gitlab.com/charlescampbell1/trello_client'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.7.1')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage

  spec.files = Dir.glob('{lib,exe}/**/*') + [
    'Gemfile',
    'README.md',
    'Rakefile',
    'VERSION'
  ]
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '>= 1.15.0', '< 3'
  spec.add_development_dependency 'bundler-audit', '~> 0.6'
  spec.add_development_dependency 'byebug', '~> 11.0'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'reek', '~> 6.0.1'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rspec_junit_formatter', '~> 0.4'
  spec.add_development_dependency 'rubocop', '~> 0.78'
  spec.add_development_dependency 'rubocop-performance', '~> 1.5'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.37', '!= 1.38.0'
  spec.add_development_dependency 'simplecov', '~> 0.16.1'
  spec.add_development_dependency 'simplecov-console', '~> 0.7.2'

  spec.add_runtime_dependency 'httparty', '~>0.18.1'
end
