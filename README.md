# Trello Client

![Build Status](https://img.shields.io/badge/build-passing-4DC729.svg)
![Coverage](https://img.shields.io/badge/coverage-100%25-4DC729.svg)
![Release](https://img.shields.io/badge/ruby-2.7.1-E01663.svg)

A simple gem for interacting with basic endpoints of the Trello RestAPI.
<https://rubygems.org/gems/trello_client_lite>

## Tracking

Track this project on the following trello board: <https://trello.com/b/TLbMg2RX/trelloclientlite>

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'trello_client_lite',
```

And then execute:

```bash
bundle install
```

Or install it yourself as:

```bash
gem install trello_client_lite
```

And require it:

```ruby
require 'trello_client_lite'
```

For usage see: <https://gitlab.com/charlescampbell1/trello_client/-/blob/master/USAGE.md>

## Contributing

This project has a few rules which applies to all contributions:

1. All commits must follow the [Convention
   Commits](https://www.conventionalcommits.org/) specification. This is less
   scary than it sounds:
   - It mostly involves adding a small prefix to each commit message that
     indicates what type of commit it is. Common types are `feat:`, `fix:`,
     `chore:`, `test:`, `docs:`, `refactor:`. See
     [here](https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional#type-enum)
     for a complete list.
   - A optional scope can also be provided. For example if adding a feature
     within the "server" scope, the commit message prefix would be
     `feat(server):`.
   - Commits which introduce breaking changes MUST have a `!` before the `:` in
     the commit message type prefix, or MUST format its main body as
     `BREAKING CHANGE: <description>`. Ideally, do both for good measure.
2. All changes must be added to `master` via Pull Requests.
3. All Pull Requests must be merged by doing a fast-forward only merge. The
   repository should already be setup to enforce this.

Following these rules enables automatic version bumping and changelog
generation, based on the commit history alone. And it also leads to a cleaner
and more structured commit history overall, as each commit needs to fit into a
specific type.

## Development

### Local Machine

1. Ensure you have Ruby version 2.5.0 or later installed.
2. Clone / checkout the repository.
3. Run `bundle install` to install dependencies.
4. Run `bundle exec rspec` to run test suite. Or alternatively, `make test` to
   run the test suite in an isolated Docker environment.
5. Run `bundle exec rubocop` to run linter. Or alternatively, `make lint` to run
   the linter in an isolated Docker environment.
6. Run `reek` to run another linter.... more details TODO
7. Make changes.

### Docker Environment

1. Clone / checkout the repository.
2. Ensure you have Docker installed with the `docker` CLI client in your `PATH`.
3. Run `make shell` to get a `sh` shell within a Alpine-based Ruby container,
   with `bundle install` already executed.
4. Test suite and linter can be run within this environment with
   `bundle exec rspec` and `bundle exec rubocop`, just like in the Local Machine
   environment.
5. Make changes.

## Release

Once enough changes have made it into `master` that it's worthy a of new
release, it's time to bump the version.

### Requirements

- Node.js
  - On macOS this can be installed with:

```bash
brew install node
```

- standard-version NPM package — Installed with:

```bash
npm install -g standard-version
```

### Cutting a Release

1. From the `master` branch, create a new branch called `release`:
   `git checkout -b release`
2. Run: `make new-version` — Under the hood this runs `npx standard-version`,
   which will:
   - Read the current version from the `VERSION` file.
   - Look at all commits since the most recent git tag to determine if those
     changes yield a MAJOR, MINOR or PATCH change.
   - Determine what the new version number is, and write said new version number
     too the `VERSION` file.
   - Update `CHANGELOG.md` with the new version, listing all `feat:` and `fix:`
     commits as items changed since the previous version.
   - Commit the changes to `VERSION` and `CHANGELOG.md` as a `chore(release):`
     type commit.
   - Create a git tag on the `chore(release):` commit matching the new version,
     with a `v` prefix.
3. Push the `release` branch, but not the git tag: `git push origin release`
4. Open a Pull Request.
5. Once the Pull Request is merged and back in master build the gem: `make build`
6. Push this gem to rubygems.org: `make release`
