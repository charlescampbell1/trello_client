# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.0](https://gitlab.com/charlescampbell1/trello_client/-/compare/v2.0.0...v2.1.0) (2020-09-11)


### Features

* **card:** Easier add card endpoint ([77f2b3c](https://gitlab.com/charlescampbell1/trello_client/-/commit/77f2b3c575932db32d22c65cf75cf89e1ef94297))

## [2.0.0](https://gitlab.com/charlescampbell1/trello_client/-/compare/v1.1.2...v2.0.0) (2020-08-11)


### ⚠ BREAKING CHANGES

* **card:** I’ve added the ability to pass more details into the card, but saw the opportunity for a little refactor to improve the quality of the interface, unfortunately this led to breaking changes.

### Features

* **card:** Add more details to card ([64af035](https://gitlab.com/charlescampbell1/trello_client/-/commit/64af035bc92d37074ce38e7f1e5477d1b51fc1d9))

### [1.1.2](https://gitlab.com/charlescampbell1/trello_client/-/compare/v1.1.1...v1.1.2) (2020-08-11)

### [1.1.1](https://gitlab.com/charlescampbell1/trello_client/-/compare/v1.1.0...v1.1.1) (2020-08-11)


### Bug Fixes

* **tools:** Correct endpoint for version compare ([20ee6d9](https://gitlab.com/charlescampbell1/trello_client/-/commit/20ee6d9d17731a2bf00c1a9c810bf3cd133d494d))

## [1.1.0](https://gitlab.com/charlescampbell1/trello_client/-/compare/v1.0.0...v1.1.0) (2020-08-11)


### Features

* **card:** Add card to a list ([2e5489a](https://gitlab.com/charlescampbell1/trello_client/-/commit/2e5489a78cc5d3cdc55347ccbdab1ac6f5b91ab9))
* **lists:** Get list by board ID ([f843507](https://gitlab.com/charlescampbell1/trello_client/-/commit/f84350724319893d8cbeaf262ba13f365b0d4a2e))

## 1.0.0 (2020-08-11)


### Features

* **boards:** Fetch board by name ([6b6e453](https://gitlab.com/charlescampbell1/trello_client/-/commit/6b6e453133e8f12c6dd94fe5ea1551eed3c3276e))


### Bug Fixes

* **tools:** Correct endpoints for changelog urls ([f61cdc0](https://gitlab.com/charlescampbell1/trello_client/-/commit/f61cdc048d950abe8435ff487f4b3631ec7bebc7))
