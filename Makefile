#
# Development Targets
#

.PHONY: test
test: Dockerfile.test test-with-bundler-1.15.1 test-with-bundler-1.17.3
	$(eval IMAGE := $(shell docker build -f Dockerfile.test -q .))
	docker run --rm \
		-v "$(shell pwd)/coverage:/app/coverage" \
		-v "$(shell pwd)/test-reports:/app/test-reports" \
		"$(IMAGE)" \
		sh -c "bundle exec rspec \
			-f progress \
			-f RspecJunitFormatter --out test-reports/rspec.xml"

.PHONY: test-with-bundler-%
test-with-bundler-%: Dockerfile.test
	$(eval IMAGE := $(shell docker build -f Dockerfile.test -q .))
	docker run --rm "$(IMAGE)" sh -c " \
		gem install bundler -v '$*' \
		&& ([ ! -f Gemfile.lock ] || rm Gemfile.lock) \
		&& bundle _$*_ config set --local without 'never' \
		&& (bundle _$*_ check || bundle _$*_ install) \
		&& env COVERAGE=0 bundle _$*_ exec rspec -f progress"

.PHONY: lint
lint: Dockerfile.test
	$(eval IMAGE := $(shell docker build -f Dockerfile.test -q .))
	docker run --rm "$(IMAGE)" sh -c "bundle exec rubocop"

.PHONY: audit
audit: Dockerfile.test
	$(eval IMAGE := $(shell docker build -f Dockerfile.test -q .))
	docker run --rm "$(IMAGE)" sh -c 'bundle audit check --update'

.PHONY: shell
shell: Dockerfile.test
	$(eval IMAGE := $(shell docker build -f Dockerfile.test -q .))
	docker run -ti --rm -v "$(shell pwd):/app" "$(IMAGE)" sh

#
# Release Targets
#

VERSION = $(shell cat VERSION)

.PHONY: new-version
new-version:
	$(if $(shell command -v npx),, \
		$(error No npx executable in PATH, please install NodeJS))
	$(if $(shell command -v standard-version),, \
		$(error No standard-version executable in PATH, \
			install with: npm i -g standard-version))

	npx standard-version

build:
	gem build trello_client_lite.gemspec

release:
	gem push trello_client_lite-$(VERSION).gem

#
# Helpers
#

DOCKER_FILES = $(wildcard Dockerfile*)

.PHONY: $(DOCKER_FILES)
$(DOCKER_FILES): Dockerfile%:
	docker build -f "Dockerfile$*" .
