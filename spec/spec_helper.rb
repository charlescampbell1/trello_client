# frozen_string_literal: true

if [nil, '', '1', 'true', 'yes'].include?(ENV['COVERAGE'])
  require 'simplecov'
  require 'simplecov-console'

  SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new(
    [
      SimpleCov::Formatter::HTMLFormatter,
      SimpleCov::Formatter::Console
    ]
  )

  SimpleCov.start 'test_frameworks'
end

require 'bundler/setup'

require 'trello'

RSpec.configure do |config|
  config.disable_monkey_patching!
  config.example_status_persistence_file_path = '.rspec_status'
  config.order = :random

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
