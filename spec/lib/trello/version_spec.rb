# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Trello do
  it 'has a version number' do
    expect(Trello::VERSION).not_to be nil
  end
end
