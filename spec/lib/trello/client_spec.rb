# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Trello::Client do
  subject(:client) do
    described_class.new(
      key: 'key',
      token: 'token'
    )
  end

  describe '#boards' do
    context 'when returning a non 200 response' do
      let(:response) { instance_double(HTTParty::Response, code: 401, body: '{}') }

      before { allow(HTTParty).to receive(:get).and_return(response) }

      it 'raises a RequestError' do
        expect { client.get_board_by_name(name: 'Foo') }.to raise_error(Trello::Exceptions::RequestError)
      end
    end

    context 'when returning a 200 response' do
      let(:response) do
        instance_double(HTTParty::Response, code: 200, body: '[{"name":"First"},{"name":"Second"}]')
      end

      before { allow(HTTParty).to receive(:get).and_return(response) }

      it 'calls off to the get boards endpoint' do
        client.get_board_by_name(name: 'Board Name')

        expect(HTTParty).to have_received(:get).with(
          'https://api.trello.com/1/members/me/boards',
          { query: { key: 'key', token: 'token' } }
        )
      end
    end
  end

  describe '#get_board_by_name' do
    let(:response) do
      instance_double(HTTParty::Response, code: 200, body: '[{"name":"First"},{"name":"Second"}]')
    end

    before { allow(HTTParty).to receive(:get).and_return(response) }

    it 'returns the board that matches the name' do
      expect(client.get_board_by_name(name: 'First')).to eq({ 'name' => 'First' })
    end
  end

  describe '#get_lists_for_board' do
    let(:response) do
      instance_double(HTTParty::Response, code: 200, body: '[{"name":"Ready"},{"name":"Done"}]')
    end

    before { allow(HTTParty).to receive(:get).and_return(response) }

    it 'calls off to the get lists endpoint' do
      client.get_lists_for_board(board_id: '123')

      expect(HTTParty).to have_received(:get).with(
        'https://api.trello.com/1/boards/123/lists',
        { query: { key: 'key', token: 'token' } }
      )
    end
  end

  describe '#get_list' do
    let(:response) do
      instance_double(HTTParty::Response, code: 200, body: '[{"name":"Ready"},{"name":"Done"}]')
    end

    before { allow(HTTParty).to receive(:get).and_return(response) }

    it 'returns the list that matches the name' do
      expect(client.get_list(board_id: '123', list_name: 'Done')).to eq({ 'name' => 'Done' })
    end
  end

  describe '#get_labels_for_board' do
    let(:response) do
      instance_double(HTTParty::Response, code: 200, body: '[{"name":"fix"},{"name":"feat"}]')
    end

    before { allow(HTTParty).to receive(:get).and_return(response) }

    it 'calls off to the get labels endpoint' do
      client.get_labels_for_board(board_id: '123')

      expect(HTTParty).to have_received(:get).with(
        'https://api.trello.com/1/boards/123/labels',
        { query: { key: 'key', token: 'token' } }
      )
    end
  end

  describe '#get_label' do
    let(:response) do
      instance_double(HTTParty::Response, code: 200, body: '[{"name":"fix"},{"name":"feat"}]')
    end

    before { allow(HTTParty).to receive(:get).and_return(response) }

    it 'returns the label that matches the name' do
      expect(client.get_label(board_id: '123', label_name: 'fix')).to eq({ 'name' => 'fix' })
    end
  end

  describe '#add_card!' do
    let(:response) { instance_double(HTTParty::Response, code: 200, body: '{"id":"5f328a84193e9f7a917186e9"}') }

    before { allow(HTTParty).to receive(:post).and_return(response) }

    context 'with the minimum requirements for a card' do
      it 'adds a card to a list' do
        client.add_card!(
          list_id: '123abc',
          title: 'API Created Card'
        )

        expect(HTTParty).to have_received(:post).with(
          'https://api.trello.com/1/cards',
          { query: {
            idList: '123abc',
            key: 'key',
            name: 'API Created Card',
            desc: nil,
            idLabels: nil,
            token: 'token'
          } }
        )
      end
    end

    context 'with all the parameters for creating a card' do
      it 'adds a card to a list' do
        client.add_card!(
          list_id: '123abc',
          title: 'API Created Card',
          description: 'The API created me',
          labels: %w[fix feat]
        )

        expect(HTTParty).to have_received(:post).with(
          'https://api.trello.com/1/cards',
          { query: {
            idList: '123abc',
            key: 'key',
            name: 'API Created Card',
            desc: 'The API created me',
            idLabels: %w[fix feat],
            token: 'token'
          } }
        )
      end
    end
  end
end
