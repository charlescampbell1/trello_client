module.exports = config = {};

config.bumpFiles = [
  {
    filename: "VERSION",
    type: "plain-text",
  },
];

const host = "https://gitlab.com/charlescampbell1";

config.commitUrlFormat = `${host}/trello_client/-/commit/{{hash}}`;

config.compareUrlFormat = `${host}/trello_client/-/compare/{{previousTag}}...{{currentTag}}`;
