# Usage

## Client

All interactions with the Trello RestAPI require that you new up a client.
Might be worth reading: <https://developer.atlassian.com/cloud/trello/guides/rest-api/api-introduction/>
to get your Trello key and token.

```ruby
client = Trello::Client.new(
   key: ENV.fetch('TRELLO_KEY'),
   token: ENV.fetch('TRELLO_TOKEN')
)
```

## Get Board

I thought the easiest way to get a board would be to pass the name of the board
in as a string like so:

```ruby
client.get_board_by_name(name: 'My Trello Board')
```

## Get Lists

Some poeple might find it useful to get the lists on a specific board, just pass
the board ID in as a string.

```ruby
client.get_lists_for_board(
  board_id: board_id
)
```

## Get List

In order to get a specific list you'll need to pass in the ID of the board the
list is on and the name of the list.

```ruby
client.get_list(
  board_id: board_id,
  list_name: 'Support'
)
```

## Get Labels

Some people might find it useful to get the labels on a specific board, just
pass the board ID in as a string.

```ruby
client.get_labels_for_board(
  board_id: board_id
)
```

## Get Label

In order to get a specific label you'll need to pass in the ID of the board the label is on and the name of the label.

```ruby
client.get_label(
  board_id: board_id,
  label_name: 'feat'
)
```

## Add Card

To add a card to a list you'll need the list ID. The minimum requirement in this
gem for adding a card is providing the list ID and a title for the card.

```ruby
client.add_card!(
  list_id: list_id,
  title: 'Try not to create breaking changes on day 1'
)
```

There are however more options you can pass in to create a card with more details.

```ruby
client.add_card!(
  list_id: '5f31d18bd29abd3351f33900',
  title: 'What breaking changes?',
  description: 'These are not the breaking changes you are looking for',
  labels: '5f31ca407669b225491b421b'
)
```

### Add Card by name

I've decided to add another endpoint instead of breaking the add_card! endpoint
for existing users. This will allow you to pass in the name of the board, list
and column you want to create the card in.

```ruby
client.add_card_by_name!(
  board_name: 'trello_client_lite',
  list_name: 'User Requests [3]',
  label_name: 'fix',
  title: 'fix: Add endpoint for getting cards',
  description: 'It would be great to have a nice endpoint for grabbing card details'
)
```

## Example

You might end up with something like this, when creating a new card with a
fix label:

```ruby
# New up the client
client = Trello::Client.new(
   key: ENV.fetch('TRELLO_KEY'),
   token: ENV.fetch('TRELLO_TOKEN')
)

# Get the board details
board = client.get_board_by_name(name: 'QuayBooks')
board_id = board['id']

# Get the list to add the card too
list = client.get_list(
  board_id: board_id,
  list_name: 'User Requests [3]'
)
list_id = list['id']

# Get the label to add to the card
label = client.get_label(
  board_id: board_id,
  label_name: 'fix'
)
label_id = label['id']

# Create a basic card
client.add_card!(
  list_id: list_id,
  title: 'An API created card'
)

# Or create a card with some detail
client.add_card!(
  list_id: list_id,
  title: 'An API created card',
  description: 'This bugfix task was created through the API',
  labels: label_id
)

# Or a card with multiple labels could look something like this
client.add_card!(
  list_id: list_id,
  title: 'An API created card',
  description: 'This bugfix task was created through the API',
  labels: [feat_id, fix_id]
)
```
