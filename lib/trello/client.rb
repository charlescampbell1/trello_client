# frozen_string_literal: true

require 'httparty'

module Trello
  class Client
    def initialize(key:, token:)
      @key = key
      @token = token
      @base_url = 'https://api.trello.com/1'
    end

    def get_board_by_name(name:)
      boards.select { |value| value['name'] == name }.first
    end

    def get_lists_for_board(board_id:)
      options = { query: { key: key, token: token } }

      request("boards/#{board_id}/lists") { |url| HTTParty.get(url, options) }
    end

    def get_list(board_id:, list_name:)
      get_lists_for_board(board_id: board_id).select { |value| value['name'] == list_name }.first
    end

    def get_labels_for_board(board_id:)
      options = { query: { key: key, token: token } }

      request("boards/#{board_id}/labels") { |url| HTTParty.get(url, options) }
    end

    def get_label(board_id:, label_name:)
      get_labels_for_board(board_id: board_id).select { |value| value['name'] == label_name }.first
    end

    def add_card!(list_id:, title:, description: nil, labels: nil)
      options = { query: { key: key, token: token, idList: list_id, name: title, desc: description, idLabels: labels } }

      request('cards') { |url| HTTParty.post(url, options) }
    end

    def add_card_by_name!(board_name:, list_name:, label_name:, title:, description: nil)
      board_id = get_board_by_name(name: board_name)['id']
      list_id = get_list(board_id: board_id, list_name: list_name)['id']
      label_id = get_label(board_id: board_id, label_name: label_name)['id']

      add_card!(
        list_id: list_id,
        title: title,
        description: description,
        labels: label_id
      )
    end

    def boards
      options = { query: { key: key, token: token } }

      request('members/me/boards') { |url| HTTParty.get(url, options) }
    end

    private

    attr_reader :base_url, :key, :token

    def request(endpoint)
      url = "#{base_url}/#{endpoint}"
      response = yield(url)

      return JSON.parse(response.body) if response.code.eql?(200)

      raise_request_error(response)
    end

    def raise_request_error(response)
      raise Trello::Exceptions::RequestError.new(
        message: response.body,
        http_code: response.code
      )
    end
  end
end
