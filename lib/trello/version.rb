# frozen_string_literal: true

module Trello
  VERSION = File.read(
    File.expand_path(File.join('..', '..', 'VERSION'), __dir__)
  ).strip
end
