# frozen_string_literal: true

module Trello
  module Exceptions
    class RequestError < StandardError
      def initialize(message:, http_code:)
        message = "#{http_code}: #{message}"
        super(message)
      end
    end
  end
end
